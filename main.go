/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>

*/
package main

import "gitlab.com/souzagusta/stress-test-cli/cmd"

func main() {
	cmd.Execute()
}
