package usecase

import (
	"log"
	"net/http"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/souzagusta/stress-test-cli/internal/dto"
)

func NewStressTestWgUseCase(input dto.StressTestInput) dto.StressTestOutput {
	var output dto.StressTestOutput

	//contador de requisições de cada status code
	requestsByCode := make(map[int]int64)
	requestByCodeMutex := sync.Mutex{}

	//registra momento de início do teste de carga
	now := time.Now()

	//total de requests que devem ser feitas
	remaining := int64(input.Requests)

	//enquanto ainda ouver requesições a serem feitas, o teste continuará
	for remaining > 0 {
		//waitGroup para esperar requisições em paralelo terminar
		var wg sync.WaitGroup

		//número de requisições que serão realizadas nesta iteração
		var requests int
		if remaining < int64(input.Concurrency) {
			//se o número de requisições restantes for menor que o número de requisições simultaneas
			//o número de requisições receberá o número restante, pois o parametro --requests precisa ser respeitado
			requests = int(remaining)
		} else {
			//caso contrário, o número de requisições receberá o número de chamadas concorrentes
			requests = input.Concurrency
		}
		log.Printf("Sending %d requests [*]\n", requests)
		//registra quantas requisições o waitGroup deve esperar
		wg.Add(requests)

		var successRequests int64 = 0

		for c := 0; c < int(requests); c++ {
			//dispara as threads em paralelo
			go func() {
				//assumindo apenas requisições GET
				response, err := http.Get(input.Url)
				if err != nil {
					log.Println(err.Error())
				} else {
					//usado atomic para threads não sobrescreverem o contador requisições que foram concluídas
					atomic.AddInt64(&successRequests, 1)

					if response.StatusCode == http.StatusOK {
						//usado atomic para threads não sobrescreverem o contador de requisições de status 200.
						atomic.AddInt64(&output.TotalRequestsStatus200, 1)
					} else {
						//realizado lock para atualizar o mapa com o contador de requisição com status != 200
						requestByCodeMutex.Lock()

						//soma +1 ao contador do status atual
						counter, exists := requestsByCode[response.StatusCode]
						if !exists {
							requestsByCode[response.StatusCode] = 1
						} else {
							requestsByCode[response.StatusCode] = counter + 1
						}
						//libera lock
						requestByCodeMutex.Unlock()
					}
				}
				//sinaliza thread concluída
				wg.Done()
			}()
		}
		//espera threads terminarem
		wg.Wait()

		//diminui o número de requisições realizadas do número de requisições restantes
		atomic.AddInt64(&remaining, int64(-successRequests))
		atomic.AddInt64(&output.TotalRequests, successRequests)
	}

	//calcula tempo total do teste de carga
	output.TimeSpend = time.Since(now).Milliseconds()
	//atribui o contador de status ao dto
	output.TotalRequestsByStatus = requestsByCode

	return output
}
