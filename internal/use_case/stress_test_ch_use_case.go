package usecase

import (
	"log"
	"net/http"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/souzagusta/stress-test-cli/internal/dto"
)

func NewStressTestChanUseCase(input dto.StressTestInput) dto.StressTestOutput {
	//contador de requisições de cada status code
	requestsByCode := make(map[int]int64)
	requestByCodeMutex := sync.Mutex{}
	//contador de requisições status 200
	var requestsStatus200 int64 = 0
	//contador de requisições completadas
	var completedRequests int64 = 0

	//registra momento de início do teste de carga
	now := time.Now()

	//canal para limitar chamadas concorrentes
	maxConcurrentRequestsLimiterChannel := make(chan bool, input.Concurrency)

	//total de requests que devem ser feitas garantidas
	remaining := input.Requests

	for remaining > 0 {

		//waitGroup para esperar requisições em paralelo terminar
		var wg sync.WaitGroup

		requests := remaining
		log.Printf("Sending %d requests [*]\n", requests)
		wg.Add(requests)

		var successRequests int64 = 0

		for r := 0; r < requests; r++ {
			// se canal possuir espaço, irá inicial requisição
			// se canal estiver cheio, irá esperar até outra requisição finalizar
			maxConcurrentRequestsLimiterChannel <- true
			go func(wg *sync.WaitGroup) {
				//sinaliza thread concluída ao final da goroutine
				defer wg.Done()
				// libera canal para outra requisição ao final da goroutine
				defer func() {
					<-maxConcurrentRequestsLimiterChannel
				}()

				//assumindo apenas requisições GET
				response, err := http.Get(input.Url)
				if err != nil {
					log.Println(err.Error())
					return
				}
				//usado atomic para threads não sobrescreverem o contador requisições que foram concluídas
				atomic.AddInt64(&successRequests, 1)

				if response.StatusCode == http.StatusOK {
					//usado atomic para threads não sobrescreverem o contador de requisições de status 200.
					atomic.AddInt64(&requestsStatus200, 1)
					return
				}
				//realizado lock para atualizar o mapa com o contador de requisição com status != 200
				requestByCodeMutex.Lock()

				//soma +1 ao contador do status atual
				counter, exists := requestsByCode[response.StatusCode]
				if !exists {
					requestsByCode[response.StatusCode] = 1
				} else {
					requestsByCode[response.StatusCode] = counter + 1
				}
				//libera lock
				requestByCodeMutex.Unlock()

			}(&wg)
		}
		wg.Wait()

		//diminui o número de requisições realizadas do número de requisições restantes
		remaining -= int(successRequests)
		//soma total de requisiçoes concluídas
		completedRequests += successRequests

		log.Printf("Success: %d\n", successRequests)
		log.Printf("Failure: %d\n", remaining)
	}

	return dto.StressTestOutput{
		//calcula tempo total do teste de carga
		TimeSpend: time.Since(now).Milliseconds(),
		//atribui o total requisições concluídas
		TotalRequests: completedRequests,
		//atribui o total de status 200
		TotalRequestsStatus200: requestsStatus200,
		//atribui o contador de status ao dto
		TotalRequestsByStatus: requestsByCode,
	}
}
