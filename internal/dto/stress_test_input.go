package dto

type StressTestInput struct {
	Url         string
	Requests    int
	Concurrency int
}
