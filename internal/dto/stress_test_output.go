package dto

import (
	"fmt"
	"strings"
)

/*
Apresentar um relatório ao final dos testes contendo:
Tempo total gasto na execução
Quantidade total de requests realizados.
Quantidade de requests com status HTTP 200.
Distribuição de outros códigos de status HTTP (como 404, 500, etc.).
*/
type StressTestOutput struct {
	//tempo total de execução do teste
	TimeSpend int64
	//total de requsições concluídas
	TotalRequests int64
	//total de requisições com status 200
	TotalRequestsStatus200 int64
	//total de requisições de cada status (200 não incluído)
	TotalRequestsByStatus map[int]int64
}

func (output *StressTestOutput) String() string {
	var params []any

	var sb strings.Builder
	sb.WriteString("Stress Test Report:\n")
	sb.WriteString("Total time: %dms\n")
	sb.WriteString("Total requests completed: %d\n")
	sb.WriteString("Total success (200) requests: %d\n")
	params = append(params, output.TimeSpend, output.TotalRequests, output.TotalRequestsStatus200)

	sb.WriteString("Total requests by status: [\n")
	for status, total := range output.TotalRequestsByStatus {
		sb.WriteString("- Status %d: %d\n")
		params = append(params, status, total)
	}
	sb.WriteString("]\n")

	return fmt.Sprintf(sb.String(), params...)
}
