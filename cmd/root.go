/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/souzagusta/stress-test-cli/internal/dto"
	usecase "gitlab.com/souzagusta/stress-test-cli/internal/use_case"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "stress-test-cli",
	Short: "Testes de carga em um serviço web",
	Long:  `Sistema CLI em Go para realizar testes de carga em um serviço web. O usuário deverá fornecer a URL do serviço, o número total de requests e a quantidade de chamadas simultâneas.`,

	Run: func(cmd *cobra.Command, args []string) {
		url, _ := cmd.Flags().GetString("url")
		requests, _ := cmd.Flags().GetInt("requests")
		concurrency, _ := cmd.Flags().GetInt("concurrency")
		sync, _ := cmd.Flags().GetString("sync")

		input := dto.StressTestInput{
			Url:         url,
			Requests:    requests,
			Concurrency: concurrency,
		}

		log.Printf("Starting stress test: Args url=%s, requests=%d, concurrency=%d, sync=%s\n",
			input.Url, input.Requests, input.Concurrency, sync)

		var output dto.StressTestOutput
		switch sync {
		case "channel":
			output = usecase.NewStressTestChanUseCase(input)
		case "wg":
			output = usecase.NewStressTestWgUseCase(input)
		default:
			output = usecase.NewStressTestChanUseCase(input)

		}

		log.Println(output.String())

	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().String("url", "", "URL do serviço a ser testado")
	rootCmd.MarkFlagRequired("url")

	rootCmd.Flags().Int("requests", 0, "Número total de requests")
	rootCmd.MarkFlagRequired("requests")

	rootCmd.Flags().Int("concurrency", 0, "Número de chamadas simultâneas")
	rootCmd.MarkFlagRequired("concurrency")

	rootCmd.Flags().String("sync", "channel", "Estratégias concorrência: wg (default) ou channel")
}
