FROM golang:1.21.6 as builder
WORKDIR /app
COPY . .
RUN GOOS=linux CGO_ENABLED=0 go build -ldflags="-w -s" -o stress-test-cli .

FROM scratch
#copia executável server para .
COPY --from=builder /app/stress-test-cli .

ENTRYPOINT [ "./stress-test-cli" ]