# Readme
* Utilizado spf13/cobra-cli:
```
go install github.com/spf13/cobra-cli@latest
```
* Teste de carga assume que serão realizadas apenas requisições do método GET
* Se a requisição falha por timeout, rede, redirect, etc, o teste refaz a requisição.
    * Ou seja, são realizadas requisições até que o total definido em --requests seja atendido;
* Ao final do teste são logadas as seguintes informações:
    * Total time: tempo total de execução do teste em milisegundos;
	* Total requests completed: total de requisições concluídas;
	* Total success (200) requests: total de requisições com status code 200;
	* Total requests by status: total de requisições de cada status (status code 200 não incluído);

# Docker build
* Executar a partir da raíz do projeto o commando:
```
docker build -t stress-test-cli .
```
# Docker run
* Executar teste de carga através do docker
```
docker run stress-test-cli --url=http://google.com --requests=1000 --concurrency=10
```
Flag `--sync={wg ou channel}` (Opcional):
* channel (Default):
	* Utiliza um channel como buffer de tamanho max igual à `--concurrency` para controlar requisições em paralelo;
	* Se o buffer estiver cheio, ou seja, atingiu o máximo de requisições em paralelo, a próxima requisição deve esperar uma requisição acabar;
* wg: 
	* Utiliza waitGroup para disparar lotes de requisição de tamanho max igual à `--concurrency` para controlar requisições em paralelo;